import java.lang.NumberFormatException

fun menu(listaProblemas:MutableList<Problemes>){
    do {
        println("Digues si ets professor(p) o alumne(a). Prem s per sortir")
        val seleccioProfOAlum = try{
            readln()
        } catch (e: NumberFormatException){
            ""
        }
        when(seleccioProfOAlum){
            "a"->menuAlumne(listaProblemas)
            "p"-> menuProfe()
            //"s" -> println("")
            else-> println("Selecció no vàlida")
        }
    }while (seleccioProfOAlum!="s")
    println("Has sortit del programa. Fins aviat")
}