import kotlinx.serialization.Serializable
import java.io.File
import java.lang.NumberFormatException

/**
 * Jutge de codi que pot adoptar dos rols: el de professor i el d'alumne.
 * Alumne -> apareixerà un menú que li permetrà fer diferents accions:
 * 1. Els problemes sortiràn per ordre, l'alumne pot decidir si intentar-lo o no i al acabar la ronda,
 * tornarà al inici i podrá tornar a intentar els problemes no resolts en ordre.
 *
 * 2. Apareixerà un llistat amb tots els problemes i pot decidir quins vol fer en el ordre que ell vulgui
 * i pot sortir també quan ell vulgui.
 *
 * 3. Aparexerà l'historial del progrés de l'alumne amb els problemes resolts, els no resolts i els intents.
 *
 * 4. Apartat d'ajuda que s'explica cada funció del menú.
 *
 * 0. Opció per sortir.
 *
 * Professor -> per accedir al seu men´u, abans ha d'introduir la contrsenya, si es correcta:
 *
 * 1. Pot afegir nous problemes, introduint cada dada d'aquest.
 *
 * 2. a partir dels problemes resolts i intents de l'alumne, pot evaluar el projecte.
 * @author Ana Gómez Pastor
 */
@Serializable
data class Problemes(
    val id: Int,
    val titol: String,
    val enunciat: String,
    val inputPublic: String,
    val outputPublic: Int,
    val inputPrivat: String,
    val outputPrivat: Int
){

}


/**
 * Main
 *
 */
fun main() {

    val listaProblemas = leerJson()


    val fileProblemes = File("dataProbl/problemes.json")
    val fileData = fileProblemes.readText()
    //val json = Json { ignoreUnknownKeys = true }
    //val lecturaProbl = json.decodeFromString<List<Problemes>>(fileData).toMutableList()
    menu(listaProblemas)

}