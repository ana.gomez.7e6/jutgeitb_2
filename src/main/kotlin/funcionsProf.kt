/**
 * Menu profe
 * FUnció que msotra el menú del professor
 */
fun mostrarMenuProfessor(){
    println("""
        ________________________________
        |MENÚ: 
        |1. Afegir problemes.
        |2. Visualitzar progrés alumnes
        |0. Sortir.
        |_______________________________
        SELECCIONA UNA OPCIÓ:
    """.trimIndent())
}

fun menuProfe(){
    println("Escriu la contrasenya de professor:")
    val contrasenya = "hola"
    val introduirContr = readln()
    if (contrasenya==introduirContr){
        do {
            mostrarMenuProfessor()
            val seleccioProfe = readln().toInt()
            when(seleccioProfe){
                1 ->{afegirProbl()}
                2 ->{evaluar()}
                0 -> println("Tornant al inici")
                else -> println("Opció no vàlida")
            }
        } while (seleccioProfe!=0)
    } else {
        println("Contrasenya incorrecta")
    }
}

fun afegirProbl(){
    println("AFEGEIX PROBLEMES")

}

fun evaluar(){
    println("Evaluar problemes")
}