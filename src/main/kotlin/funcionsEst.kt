/**
 * Menu alumne
 * Funció que mostra el menú de l'alumne
 * @param nomAlumne
 */
fun mostrarMenuALumne(nomAlumne:String){
    println("""
        _______________________________
        |MENÚ
        |Alumne: $nomAlumne
        |1. Ronda de problemes.
        |2. Llista de problemes.
        |3. Consultar historial.
        |4. Ajuda.
        |0. Sortir.
        |_______________________________
        SELECCIONA UNA OPCIÓ:
    """.trimIndent())
}

fun menuAlumne(llistaProblemes: MutableList<Problemes>){
    println("Has seleccionat alumne. Escriu el teu nom:")
    val nomAlumne = readln().toString()
    do {
        mostrarMenuALumne(nomAlumne)
        val seleccioAlumne = readln().toInt()
        when(seleccioAlumne){
            1 ->{rondaProbl(llistaProblemes)}
            2 ->{llistatProbl()}
            3 ->{/*historial()*/}
            4 ->{ajudaAlumne()}
            0 -> println("Tornant al inici")
            else -> println("Opció no vàlida")
        }
    }while (seleccioAlumne!=0)
}

fun rondaProbl(llistaProblemes: MutableList<Problemes>){
    println("Ronda de problemes")

    val exerciciIndex=0
    var sortr=false
    while (!sortr){
        val problema = llistaProblemes[exerciciIndex]
        println(problema.enunciat)
        //TODO mostrar inpu y output

        print("Vols resoldre?0,1,2")

        var ferProblema = readln().toInt()

        when (ferProblema){
            1 -> resoldreProblema(problema)
            2 ->{
                //TODO exerciciIndex++
            }

            0->sortr=true
        }

    }

}

fun resoldreProblema(problema: Problemes){
    //TODO
}

fun llistatProbl(){
    println("Llistat de problemes")

}

/*fun historial(){
    val pathAlumne = Path("dataProbl/dataAlumne.txt")
    val lectura = pathAlumne.readLines()
    println("Consulata el teu historial de problemes:")
    println(lectura)
}*/

/**
 * Ajuda alumne
 * Funció que mostra ajuda a l'alummne i com funciona el jutge
 */
fun ajudaAlumne(){
    println("""
        AJUDA.
        En el menú principal pots observar diferents accions:
        
        1. Et sortirán els 20 problemes en ordre, amb el seu respectiu enunciat i exemples. 
        Pots decidir si fer-lo o deixar-lo i passar al següent. Si l'intentas tens tants intents
        com vulguis, però tingues present que es descomptaràn a la nota. Un cop es completi la 
        primera volta, podrás tornar a fer els que no has resolt.
        
        2. Et sortirá un llistat amb tots els exercicis i pots seleccionar quins vols fer. Pots
        intentar-los els cops que necessitis, però a més intents, major penalització. Pots sortir 
        quan vulguis i intentar-ho un altre cop, el teu progrés es guardarà.
        
        3. Aquí podrás veure el teu progrés fins ara, els problemes que has resolt, quins no, els
        intents que has realitzat i si el professor t'ha puntuat.
        
        4. L'apartat d'ajuda, on et trobes ara.
        
        Prem 0 per sortir.
        
    """.trimIndent())
}

